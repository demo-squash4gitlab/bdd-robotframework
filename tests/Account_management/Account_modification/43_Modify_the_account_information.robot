*** Settings ***
Documentation    Modify the account information
...
...              This test case verifies the successful account information modification, 
...              ensuring the correct display of the success message and proper handling of 
...              the privacy policy and GDPR settings.
Metadata         ID                           43
Metadata         Reference                    ACC_TC_006
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Modify the account information
    [Documentation]    Modify the account information

    &{dataset} =    Retrieve Dataset

    Given I created an account with gender "M" firstName "john" lastName "doe" email "johndoe@mail.com" password "pass1234" birthDate "01/01/1950" partnerOffers "yes" newsletter "yes"
    And I am logged in with email "johndoe@mail.com" and password "pass1234"
    And I am on the "MyIdentity" page
    When I fill MyIdentity fields with gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" email "${dataset}[mail]" oldPass "pass1234" newPass "${dataset}[password]" birthDate "${dataset}[birth]" partnerOffers "${dataset}[offers]" privacyPolicy "yes" newsletter "${dataset}[news]" gdpr "yes" and submit
    Then The message "Information mise à jour avec succès." should be displayed
    And My personal information should be gender "${dataset}[gender]" firstName "${dataset}[first]" lastName "${dataset}[last]" email "${dataset}[mail]" birthDate "${dataset}[birth]" acceptPartnerOffers "${dataset}[offers]" acceptPrivacyPolicy "no" acceptNewsletter "${dataset}[news]" acceptGdpr "no"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_43_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_43_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_43_SETUP_VALUE} =    Get Variable Value    ${TEST_43_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_43_SETUP_VALUE is not None
        Run Keyword    ${TEST_43_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_43_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_43_TEARDOWN}.

    ${TEST_43_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_43_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_43_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_43_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${gender} =      Get Test Param    DS_gender
    ${first} =       Get Test Param    DS_first
    ${last} =        Get Test Param    DS_last
    ${mail} =        Get Test Param    DS_mail
    ${password} =    Get Test Param    DS_password
    ${birth} =       Get Test Param    DS_birth
    ${offers} =      Get Test Param    DS_offers
    ${news} =        Get Test Param    DS_news

    &{dataset} =    Create Dictionary    gender=${gender}        first=${first}    last=${last}        mail=${mail}
    ...                                  password=${password}    birth=${birth}    offers=${offers}    news=${news}

    RETURN    &{dataset}
