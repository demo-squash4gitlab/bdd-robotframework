*** Settings ***
Documentation    Add two products to the cart
...
...              This test case verifies the successful addition of two products to the 
...              cart, displaying the product names, quantities, dimensions, and prices.
Metadata         ID                           48
Metadata         Reference                    CART_TC_003
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Add two products to the cart
    [Documentation]    Add two products to the cart

    &{datatables} =    Retrieve Datatables

    Given I am logged in
    And I am on the "Home" page
    When I navigate to category "art"
    And I navigate to product "Affiche encadrée The best is yet to come"
    And I add to cart
    And I navigate to category "art"
    And I navigate to product "Illustration vectorielle Renard"
    And I add to cart
    Then The cart should contain "${datatables}[datatable_1]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_48_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_48_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_48_SETUP_VALUE} =    Get Variable Value    ${TEST_48_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_48_SETUP_VALUE is not None
        Run Keyword    ${TEST_48_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_48_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_48_TEARDOWN}.

    ${TEST_48_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_48_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_48_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_48_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                                     Number    Dimension
    @{row_1_2} =    Create List    Affiche encadrée The best is yet to come    1         40x60cm
    @{row_1_3} =    Create List    Illustration vectorielle Renard             1
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}

    RETURN    &{datatables}
